package com.example.sessionmanager;

public class ToDoList {

    private String id;
    private String relativeXPos;
    private String relativeYPos;

    public ToDoList(){
        this.id = "DEFAULT_ID";
        this.relativeXPos = "DEFAULT_XPOS";
        this.relativeYPos = "DEFAULT_YPOS";
    }

    public ToDoList(String id, String relativeXPos, String relativeYPos){
        this.id = id;
        this.relativeXPos = relativeXPos;
        this.relativeYPos = relativeYPos;
    }



    public String getRelativeXPos() {
        return relativeXPos;
    }

    public void setRelativeXPos(String relativeXPos) {
        this.relativeXPos = relativeXPos;
    }

    public String getRelativeYPos() {
        return relativeYPos;
    }

    public void setRelativeYPos(String relativeYPos) {
        this.relativeYPos = relativeYPos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
