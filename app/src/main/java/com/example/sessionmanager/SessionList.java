package com.example.sessionmanager;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;


public class SessionList extends ArrayAdapter {

    private Activity context;
    private List<Session> sessionList;

    public SessionList(Activity context, List<Session> sessionList){
        super(context, R.layout.session_list, sessionList);
        this.context = context;
        this.sessionList = sessionList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.session_list,null,true);
        TextView textViewId = (TextView) listViewItem.findViewById(R.id.textViewId);




        Session artist = sessionList.get(position);
        textViewId.setText(artist.getSessionId());



        return listViewItem;
    }
}
