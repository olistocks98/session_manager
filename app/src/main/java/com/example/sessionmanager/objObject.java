package com.example.sessionmanager;

public class objObject {

    private String id;
    private String url;
    private String relativeXPos;
    private String relativeYPos;

    public objObject(){
        this.id = "DEFAULT_ID";
        this.url = "DEFAULT_URL";
        this.relativeXPos = "DEFAULT_XPOS";
        this.relativeYPos = "DEFAULT_YPOS";
    }

    public objObject(String id, String url, String relativeXPos, String relativeYPos){
        this.id = id;
        this.url = url;
        this.relativeXPos = relativeXPos;
        this.relativeYPos = relativeYPos;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRelativeXPos() {
        return relativeXPos;
    }

    public void setRelativeXPos(String relativeXPos) {
        this.relativeXPos = relativeXPos;
    }

    public String getRelativeYPos() {
        return relativeYPos;
    }

    public void setRelativeYPos(String relativeYPos) {
        this.relativeYPos = relativeYPos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

