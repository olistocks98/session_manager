package com.example.sessionmanager;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.sessionmanager.R;
import com.example.sessionmanager.ToDoList;

import java.util.List;

public class ToDoListList extends ArrayAdapter<ToDoList> {
    private Activity context;
    private List<ToDoList> toDoLists;

    public ToDoListList(Activity context, List<ToDoList> toDoLists){
        super(context, R.layout.to_do_list_list, toDoLists);
        this.context = context;
        this.toDoLists = toDoLists;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.to_do_list_list,null,true);
        TextView textViewId = (TextView) listViewItem.findViewById(R.id.textViewId);





        ToDoList toDoList = toDoLists.get(position);
        textViewId.setText(toDoList.getId());

        return listViewItem;
    }
}
