package com.example.sessionmanager;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.sessionmanager.R;
import com.example.sessionmanager.ToDoList;

import java.util.List;

public class ObjObjectList extends ArrayAdapter<objObject> {
    private Activity context;
    private List<objObject> objObjects;

    public ObjObjectList(Activity context, List<objObject> objObjects){
        super(context, R.layout.obj_object_list, objObjects);
        this.context = context;
        this.objObjects = objObjects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.obj_object_list,null,true);
        TextView textViewId = (TextView) listViewItem.findViewById(R.id.textViewID);





        objObject objObject = objObjects.get(position);
        textViewId.setText(objObject.getId());

        return listViewItem;
    }
}
