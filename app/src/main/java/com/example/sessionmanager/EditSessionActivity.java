package com.example.sessionmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Snapshot;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class EditSessionActivity extends AppCompatActivity {

    TextView textViewSessionId;
    EditText editTextUpdateCloudAnchor;
    Button buttonDeleteSession;
    Button buttonNewToDoList;
    Button buttonNewObject;
    Button buttonUpdateCloudAnchor;
    ListView listViewToDoLists;
    ListView listViewObjects;
    List<ToDoList> toDoLists;
    List <objObject> objObjects;
    String cloudAnchor;
    String sessionId;

    DatabaseReference databaseOpenSession;
    DatabaseReference databaseOpenSessionObjObjects;
    DatabaseReference databaseOpenSessionToDoLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_session);

        textViewSessionId = findViewById(R.id.textViewSessionId);
        editTextUpdateCloudAnchor = findViewById(R.id.editTextCloudAnchor);
        buttonDeleteSession = findViewById((R.id.buttonDeleteSession));
        buttonNewToDoList = findViewById(R.id.buttonNewToDoList);
        buttonNewObject = findViewById(R.id.buttonNewObject);
        listViewToDoLists = findViewById(R.id.listViewObjObjects);
        listViewObjects = findViewById(R.id.listViewObjObjects);
        buttonUpdateCloudAnchor = findViewById(R.id.buttonUpdateCloudAnchor);

        Intent intent = getIntent();

        toDoLists = new ArrayList<>();
        objObjects = new ArrayList<>();

        sessionId = intent.getStringExtra(MainActivity.SESSION_ID);
        cloudAnchor = intent.getStringExtra(MainActivity.CLOUD_ANCHOR);

        textViewSessionId.setText(sessionId);
        editTextUpdateCloudAnchor.setText(cloudAnchor);

        databaseOpenSession = FirebaseDatabase.getInstance().getReference("Sessions").child(sessionId);
        databaseOpenSessionObjObjects = databaseOpenSession.child("Objects");
        databaseOpenSessionToDoLists = databaseOpenSession.child("To Do Lists");

        buttonDeleteSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSession();
            }
        });

        buttonUpdateCloudAnchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCloudAnchor();
            }
        });

        buttonNewToDoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newToDoList();
            }
        });

        buttonNewObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newObject();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseOpenSessionObjObjects.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                objObjects.clear();

                for(DataSnapshot objObjectSnapshot : dataSnapshot.getChildren()){
                    objObject objObject = objObjectSnapshot.getValue(objObject.class);
                    objObjects.add(objObject);
                }

                ObjObjectList objObjectListAdapter = new ObjObjectList(EditSessionActivity.this, objObjects);
                listViewObjects.setAdapter(objObjectListAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void deleteSession(){
        databaseOpenSession.removeValue();
        super.finish();
    }

    private void updateCloudAnchor(){
        String newCloudAnchor = editTextUpdateCloudAnchor.getText().toString().trim();
        databaseOpenSession.child("cloudAnchor").setValue(newCloudAnchor);
    }

    private void newObject(){
        String id = databaseOpenSessionObjObjects.push().getKey();
        objObject objObject = new objObject(id, "URL", "Relative X", "Relative Y");
        databaseOpenSessionObjObjects.child(id).setValue(objObject);
        Toast.makeText(this, "Object Created", Toast.LENGTH_LONG).show();
    }

    private void newToDoList(){
        String id = databaseOpenSessionToDoLists.push().getKey();
        ToDoList toDoList = new ToDoList(id, "Relative X", "Relative Y");
        databaseOpenSessionToDoLists.child(id).setValue(toDoList);
        Toast.makeText(this, "Object Created", Toast.LENGTH_LONG).show();
    }
}
