package com.example.sessionmanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Snapshot;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String SESSION_ID = "sessionid";
    public static final String CLOUD_ANCHOR = "cloudanchor";


   //Initialise Resources
    EditText editTextCloudAnchor;
    Button buttonNewSession;
    ListView listViewSessions;

    //Initialise the list of sessions
    List<Session> sessionList;

    //Initialise the reference to the database
    DatabaseReference databaseSessions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        editTextCloudAnchor = (EditText) findViewById(R.id.editTextCloudAnchor);
        buttonNewSession = (Button) findViewById(R.id.buttonNewSession);
        listViewSessions = (ListView) findViewById(R.id.listViewSessions);
        sessionList = new ArrayList<>();
        databaseSessions = FirebaseDatabase.getInstance().getReference("Sessions");


        //Click action for the new session button
        buttonNewSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newSession();
            }
        });

        listViewSessions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Session session = sessionList.get(position);
                Intent intent = new Intent(getApplicationContext(), EditSessionActivity.class);
                intent.putExtra(SESSION_ID, session.getSessionId());
                intent.putExtra(CLOUD_ANCHOR, session.getCloudAnchor());
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseSessions.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                sessionList.clear();
                for(DataSnapshot sessionSnapshot : dataSnapshot.getChildren()){
                    Session session = sessionSnapshot.getValue(Session.class);
                    sessionList.add(session);
                }

                SessionList adapter = new SessionList(MainActivity.this, sessionList);
                listViewSessions.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // Add a new session to the database
    private void newSession(){
        String cloudAnchor = editTextCloudAnchor.getText().toString().trim();
        if(TextUtils.isEmpty(cloudAnchor)){
            cloudAnchor = "NULL";
        }
        String id = databaseSessions.push().getKey();
        Session session = new Session(id, cloudAnchor);
        databaseSessions.child(id).setValue(session);
        Toast.makeText(this, "Session Created", Toast.LENGTH_LONG);
    }

}
