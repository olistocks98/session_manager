package com.example.sessionmanager;

public class Session {
    String sessionId;
    String cloudAnchor;

    public Session(){
    }

    public Session(String sessionId, String cloudAnchor){
        this.sessionId = sessionId;
        this.cloudAnchor = cloudAnchor;
    }


    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCloudAnchor() {
        return cloudAnchor;
    }

    public void setCloudAnchor(String cloudAnchor) {
        this.cloudAnchor = cloudAnchor;
    }
}
